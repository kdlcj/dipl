\documentclass{article}
\usepackage{fontspec}
\usepackage{chronology}
\usepackage{tcolorbox}
\usepackage{eucal}
\usepackage{amsthm}
\usepackage{afterpage}
\usepackage{enumitem}
\usepackage{adforn}
\usepackage[a4paper, margin=45mm]{geometry}
\usepackage[english]{babel}
\usepackage{xcolor}
\usepackage{microtype}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage[backend=biber]{biblatex}
\usepackage{amssymb}
\usepackage[hidelinks, pdftitle={Bifurcation Analysis of Boolean Networks}, pdfauthor={Kadlecaj, Jakub}]{hyperref}

\usepackage{tikz-cd}
\usepackage{adjustbox}
\usetikzlibrary{arrows,automata}

\addbibresource{bib.bib}

%\definecolor{RED}{HTML}{8C0D0F}
%\definecolor{RED}{HTML}{0F3669}
\definecolor{RED}{HTML}{134977}
\setsansfont{Bailey Sans ITC TT}

\setmainfont[Ligatures={Common}]{EB Garamond}
\setmathrm{CMU Serif}


\newfontface\Swash{EB Garamond Italic}[Style=Swash, Ligatures=Rare]
\newcommand\textsw[1]{{\Swash#1}}

\setmonofont{Fira Mono}
\newcommand{\tl}[1]{\fontspec[SizeFeatures={Size=20}, Style=TitlingCaps]{Adobe Garamond Pro}\selectfont #1}
\usepackage{listings}

\newcommand{\stability}{\ensuremath{\odot}}
\newcommand{\oscillation}{\ensuremath{\circlearrowleft}}
\newcommand{\disorder}{\ensuremath{\rightleftarrows}}
\newcommand{\cit}[1]{\textsuperscript{\cite{#1}}}

\newcommand{\state}[1]{\raisebox{-1.7mm}{\includegraphics[width=0.54cm]{figures/configs/#1.pdf}}}

\newcounter{definitionCounter}
\setcounter{definitionCounter}{0}
\newcommand{\definition}[0]{\paragraph{Definition \arabic{definitionCounter}.}\stepcounter{definitionCounter}}

\newcommand{\aeon}[0]{\textsf{Aeon}}


\lstset{
    basicstyle=\ttfamily,
    mathescape,
    extendedchars=true
    inputencoding=utf8,
}

\begin{document}
\Huge
$\disorder \stability \oscillation$
\end{document}
