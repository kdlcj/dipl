#!/usr/bin/python3

import os
from datetime import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.cbook as cbook
import numpy as np

os.system('git log > log.txt')

log = open('log.txt', 'r').readlines()

information = []
final_date = []
final_chars = []

line = ''
commit = ''
for line in log:
    if line[:6] == 'commit':
        commit = line.split()[1].strip()
    if line[:4] == 'Date':
        date = line[5:].strip()
        information.append((commit, date))

for commit, date in information:
    print(date)
    os.system('git show ' + commit + ':' + 'dipl.pdf > text.pdf')
    os.system('pdf2txt text.pdf > text.txt && wc -c text.txt > characters.txt')
    chars = int(open('characters.txt').read().strip().split()[0])
    date = datetime.strptime(date, '%a %b %d %H:%M:%S %Y %z')
    print('  ', chars, ' characters')
    final_date.append(date)
    final_chars.append(chars)

print(final_date, final_chars)

plt.plot_date(final_date, final_chars, 'go--')


plt.savefig('latest_progress.pdf')
plt.gca().xaxis.set_major_locator(mdates.DayLocator())

plt.show()


