#!/usr/bin/python

import matplotlib.pyplot as plt
import numpy

lines  = open('thread.txt', 'r').readlines()
#lines  = open('measure_c1.txt', 'r').readlines()
#lines  = open('measure_c1.txt', 'r').readlines()
#lines2 = open('measure_c256.txt', 'r').readlines()

print('lines size is', len(lines))

x = []
y = []

x2 = []
y2 = []


for line in lines:
    split = line.split()
    if len(split) != 2:
        continue 

    x.append(int(split[0]))
    y.append(float(split[1]))

#for line in lines2:
#    split = line.split()
#    if len(split) != 2:
#        continue 
#
#    x2.append(int(split[0]))
#    y2.append(float(split[1]))

plt.plot(x,y, 'o')

yy = []
for a,i in enumerate(y):
    yy.append(35.3735/ x[a])

plt.plot(x,yy, 'o-')

# 35.3735

#speedup = []
#for i,a in enumerate(y):
#    speedup.append(i * a)
#
#plt.plot(x, speedup, 'o-')

#plt.xscale('log', basex=2)
#plt.yscale('log', basey=2)
plt.xticks(range(35))
#plt.yticks([*map(lambda x: 2**x, range(-10, 10))])
#plt.yticks([*map(lambda x: 10*x, range(0, 45))])

#print("correlation is", numpy.corrcoef(x, y)[0, 1])


plt.savefig('measure_fig.svg', format='svg')
plt.savefig('measure_fig.pdf', format='pdf')
plt.show()

