
main:
	xelatex dipl.xtx
all:
	xelatex dipl.xtx
	biber dipl
	xelatex dipl.xtx

clear:
	rm *.aux *.log *.blg *.toc

vim:
	make all; vim dipl.xtx
