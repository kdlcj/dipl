\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1}Historical background}{1}{subsection.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Attractors}{2}{section*.6}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Bifurcation analysis}{2}{section*.7}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Parameters}{3}{section*.8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2}Novelty of the approach}{3}{subsection.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Preliminaries}{4}{section.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Non-parametrized Boolean networks}{4}{subsection.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Definition 1.}{4}{section*.9}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Definition 2.}{5}{section*.10}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Definition 3.}{6}{section*.11}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Definition 4.}{7}{section*.12}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Definition 5.}{8}{section*.13}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Definition 6.}{8}{section*.14}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Parametrized Boolean networks}{9}{subsection.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Definition 7.}{9}{section*.15}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Regulation classification constraints}{10}{section*.16}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Definition 8.}{11}{section*.17}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3}Problem statement}{12}{subsection.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}The algorithm}{13}{section.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Semi-symbolic representation}{13}{subsection.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Representing parametrizations}{13}{section*.18}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Binary decision diagrams}{14}{section*.19}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Construction}{14}{section*.20}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Properties}{14}{section*.21}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Sets as functions}{15}{section*.22}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Representing the state transition graph}{15}{section*.23}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}Computing valid parametrizations}{16}{subsection.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Input and output specification}{16}{section*.24}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Monotonicity}{16}{section*.25}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Observability}{16}{section*.26}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3}Constructing the parametrized state transition graph}{18}{subsection.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Input and output specification}{18}{section*.27}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Procedure}{18}{section*.28}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4}Attractor detection}{19}{subsection.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Naïve approach}{19}{section*.29}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Attractors in non-parametrized graph}{19}{section*.30}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Parametrized reachability}{21}{section*.31}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Pivot selection}{22}{section*.32}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.5}Attractor classification}{23}{subsection.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Input and output specification}{23}{section*.33}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{The procedure}{23}{section*.34}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}The tool and its features}{24}{section.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1}Getting the tool running}{25}{subsection.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{The engine}{25}{section*.35}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{The client}{25}{section*.36}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2}Creating and editing models}{26}{subsection.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Graphical editor}{26}{section*.37}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Model editor panel}{26}{section*.38}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3}File import and export}{27}{subsection.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\textsf {Aeon}{} file format}{27}{section*.39}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Systems Biology Markup Language}{28}{section*.40}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Local storage}{28}{section*.41}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4}Model analysis}{29}{subsection.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Results and witnesses}{29}{section*.42}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Attractor visualization}{30}{section*.43}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.5}The help panel and the manual}{30}{subsection.4.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Implementation}{31}{section.5}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Attribution}{31}{section*.44}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1}Client}{31}{subsection.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Document object model}{31}{section*.45}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Network visualization}{31}{section*.46}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Attractor visualization}{31}{section*.47}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{General architecture}{32}{section*.48}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{{\relax \fontsize {9}{11}\selectfont \abovedisplayskip 8.5\p@ plus3\p@ minus4\p@ \abovedisplayshortskip \z@ plus2\p@ \belowdisplayshortskip 4\p@ plus2\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 4\p@ plus2\p@ minus2\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \texttt {ComputeEngine}}}{32}{section*.49}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\relax \fontsize {9}{11}\selectfont \abovedisplayskip 8.5\p@ plus3\p@ minus4\p@ \abovedisplayshortskip \z@ plus2\p@ \belowdisplayshortskip 4\p@ plus2\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 4\p@ plus2\p@ minus2\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \texttt {Results}}{32}{section*.50}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\relax \fontsize {9}{11}\selectfont \abovedisplayskip 8.5\p@ plus3\p@ minus4\p@ \abovedisplayshortskip \z@ plus2\p@ \belowdisplayshortskip 4\p@ plus2\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 4\p@ plus2\p@ minus2\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \texttt {LiveModel}}{32}{section*.51}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\relax \fontsize {9}{11}\selectfont \abovedisplayskip 8.5\p@ plus3\p@ minus4\p@ \abovedisplayshortskip \z@ plus2\p@ \belowdisplayshortskip 4\p@ plus2\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 4\p@ plus2\p@ minus2\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \texttt {ModelEditor}}{32}{section*.52}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\relax \fontsize {9}{11}\selectfont \abovedisplayskip 8.5\p@ plus3\p@ minus4\p@ \abovedisplayshortskip \z@ plus2\p@ \belowdisplayshortskip 4\p@ plus2\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 4\p@ plus2\p@ minus2\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \texttt {CytoscapeEditor}}{32}{section*.53}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\relax \fontsize {9}{11}\selectfont \abovedisplayskip 8.5\p@ plus3\p@ minus4\p@ \abovedisplayshortskip \z@ plus2\p@ \belowdisplayshortskip 4\p@ plus2\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 4\p@ plus2\p@ minus2\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \texttt {UI}}{32}{section*.54}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2}Compute engine}{33}{subsection.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\relax \fontsize {9}{11}\selectfont \abovedisplayskip 8.5\p@ plus3\p@ minus4\p@ \abovedisplayshortskip \z@ plus2\p@ \belowdisplayshortskip 4\p@ plus2\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 4\p@ plus2\p@ minus2\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \texttt {lib bdd}}{33}{section*.55}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\relax \fontsize {9}{11}\selectfont \abovedisplayskip 8.5\p@ plus3\p@ minus4\p@ \abovedisplayshortskip \z@ plus2\p@ \belowdisplayshortskip 4\p@ plus2\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 4\p@ plus2\p@ minus2\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \texttt {lib pbn}}{33}{section*.56}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\relax \fontsize {9}{11}\selectfont \abovedisplayskip 8.5\p@ plus3\p@ minus4\p@ \abovedisplayshortskip \z@ plus2\p@ \belowdisplayshortskip 4\p@ plus2\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 4\p@ plus2\p@ minus2\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \texttt {tscc search}}{33}{section*.57}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\relax \fontsize {9}{11}\selectfont \abovedisplayskip 8.5\p@ plus3\p@ minus4\p@ \abovedisplayshortskip \z@ plus2\p@ \belowdisplayshortskip 4\p@ plus2\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 4\p@ plus2\p@ minus2\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \texttt {tscc classify}}{33}{section*.58}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3}The server and its interface}{34}{subsection.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Cross-origin resource sharing}{34}{section*.59}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Message format}{34}{section*.60}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Server endpoints}{35}{section*.61}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}Evaluation}{36}{section.6}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Compatibility}{36}{section*.62}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Performance}{36}{section*.63}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Parallelism}{37}{section*.64}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Scaling in state space}{38}{section*.65}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Scaling in parameter space}{39}{section*.66}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Variance}{40}{section*.67}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7}Conclusion}{41}{section.7}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Desiderata}{41}{section*.68}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A}\textsf {Aeon}{} file format specification}{44}{appendix.A}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {B}The \textsf {Aeon}{} manual}{45}{appendix.B}
